# flatpak-nvidia-manager

A script to install the versions of `org.freedesktop.Platform.GL.nvidia` and `org.freedesktop.Platform.GL32.nvidia` corresponding to your installed version of the proprietary NVIDIA driver and remove outdated versions. Currently only supports Pacman.

## Licensing

> Copyright (C) 2024 Joëlle van Essen
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU General Public License for more details.
>
> You should have received [a copy of the GNU General Public License](LICENSE.md)
> along with this program. If not, see <https://www.gnu.org/licenses/>.